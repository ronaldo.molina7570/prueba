-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2022 a las 06:03:30
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `teminal2022`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bus`
--

CREATE TABLE `bus` (
  `id_bus` bigint(11) NOT NULL,
  `nombre_bus` varchar(300) DEFAULT NULL,
  `numero_bus` varchar(20) DEFAULT NULL,
  `numero_placa_bus` varchar(20) DEFAULT NULL,
  `nombre_chofer_bus` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bus`
--

INSERT INTO `bus` (`id_bus`, `nombre_bus`, `numero_bus`, `numero_placa_bus`, `nombre_chofer_bus`) VALUES
(1, NULL, NULL, NULL, NULL),
(2, 'cotopaxi', '234', '4556', NULL),
(3, 'hyy', '4455', '663', NULL),
(4, NULL, NULL, NULL, NULL),
(5, 'rrrr', '455', '455', 'joe'),
(6, 'ggggg', '456', '56', 'jose'),
(7, 'ggggg', '566', 'h6', 'pedro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` bigint(11) NOT NULL,
  `nombres_cliente` varchar(150) NOT NULL,
  `apellidos_cliente` varchar(150) NOT NULL,
  `cedula_cliente` varchar(10) NOT NULL,
  `correo_cliente` varchar(150) NOT NULL,
  `fecha_nacimiento_cliente` date NOT NULL,
  `genero_cliente` varchar(100) NOT NULL,
  `direccion_cliente` varchar(150) NOT NULL,
  `celular_cliente` varchar(10) NOT NULL,
  `pass_cliente` varchar(250) NOT NULL,
  `rol_cliente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombres_cliente`, `apellidos_cliente`, `cedula_cliente`, `correo_cliente`, `fecha_nacimiento_cliente`, `genero_cliente`, `direccion_cliente`, `celular_cliente`, `pass_cliente`, `rol_cliente`) VALUES
(6, 'pedro', '2', '2', 'q@q', '2022-02-17', 'Masculino', 'asd', '12', 'CLIENTE', 'CLIENTE'),
(7, '1', '2', '3', 'e@q', '2022-02-09', 'Masculino', 'dsa', '321', 'tre', 'CLIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cooperativa`
--

CREATE TABLE `cooperativa` (
  `id_coop` bigint(11) NOT NULL,
  `nombre_coop` varchar(150) NOT NULL,
  `propietario_coop` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` bigint(11) NOT NULL,
  `cedula_usuario` varchar(10) NOT NULL,
  `nombres_usuario` varchar(100) NOT NULL,
  `apellidos_usuario` varchar(150) NOT NULL,
  `correo_usuario` varchar(150) NOT NULL,
  `fecha_nacimiento_usuario` date NOT NULL,
  `genero_usuario` varchar(50) NOT NULL,
  `direccion_usuario` varchar(150) NOT NULL,
  `celular_usuario` varchar(11) NOT NULL,
  `pass_usuario` varchar(200) NOT NULL,
  `rol_usuario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `cedula_usuario`, `nombres_usuario`, `apellidos_usuario`, `correo_usuario`, `fecha_nacimiento_usuario`, `genero_usuario`, `direccion_usuario`, `celular_usuario`, `pass_usuario`, `rol_usuario`) VALUES
(1, '1', 'Ronaldo', '12', 'd@q', '2022-02-17', 'Femenino', 'sAS', '12', '12', 'Secretaria'),
(3, '1234567890', 'juan', 'perez', 'ronaldo.molina7570@utc.edu.ec', '2022-03-04', 'Masculino', 'ambato', '0995479529', 'admin2022', 'Administrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `cooperativa`
--
ALTER TABLE `cooperativa`
  ADD PRIMARY KEY (`id_coop`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cooperativa`
--
ALTER TABLE `cooperativa`
  MODIFY `id_coop` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
