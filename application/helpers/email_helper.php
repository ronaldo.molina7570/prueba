<?php
function eviarEmail($emailDestinatario, $asuntoEmail, $contenidoEmail, $adjuntoEmail=""){
    try {
      //proceso de envio de correo electronico
      $CI=&get_instance();
      $CI->load->library('email');
      $configuracionCorreo = array(
             'protocol' => 'smtp',
             'smtp_host' => 'smtp.gmail.com',
             'smtp_port' => '587',
             '_smtp_auth'=>TRUE,
             'smtp_crypto' => 'tls',
             'smtp_user' => 'ronaldo.molina.salas@gmail.com',
             'smtp_pass' => 'Sistemas2021',
             'mailtype' => 'html',
             'send_multipart' => TRUE,
             'wordwrap'=>TRUE,
             'charset' => 'utf-8',
             'newline' => "\r\n",
             'ssl' => array(
              'verify_peer' => false,
              'verify_peer_name' => false,
              'allow_self_signed' => true
             )
           );
           //iniciamos el proceso de envio del email
           //bajo la configuracion establecida
           $CI->email->initialize($configuracionCorreo);
           //definiendo saltos de linea en el contenido del email
           $CI->email->set_newline("\r\n");
           //definiendo el email del cual va a salir el correo(remitente)
           $CI->email->from("ronaldo.molina.salas@gmail.com");
           //definiendo el email al donde va a llegar el correo
           $CI->email->to($emailDestinatario);
           //definiendo el asunto
           $CI->email->subject($asuntoEmail);
           //definiedno el contenido del mensaje
           $CI->email->message($contenidoEmail);
           if ($adjuntoEmail!="") {
             $CI->email->attach($adjuntoEmail);
           }
           //enviando el correo electronico bajo la configuracion establecida
           //y con los parametros indicados
           $CI->email->send();
           var_dump($CI->email->print_debugger());
    } catch (Exception $ex) {
      echo "<h1>Error al enviar el correo $ex </h1>";
    }

}
?>
