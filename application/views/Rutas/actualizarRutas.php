<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Rutas</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="row">
    <div class="col-md-12 text-center" >
        <legend style="color: #fff">
            ACTUALIZAR DATOS DE LAS RUTAS
        </legend>
    </div>
</div>
    <br>
    <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <form  class="" action="<?php echo site_url(); ?>/rutas/actualizar_Ruta" method="post" id="formulario_nueva_ruta">
                <input type="hidden" name="id_ruta" id="id_ruta" class="form-control" value="<?php echo $actualizar_Ruta->id_ruta; ?>" required >
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Lugar Salida:</label>
                  <input type="text" class="form-control" name="lugar_salida_ruta" id="lugar_salida_ruta" value="<?php echo $actualizar_Ruta->lugar_salida_ruta; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">lugar Destino:</label>
                  <input type="text" class="form-control" name="lugar_destino_ruta" id="lugar_destino_ruta" value="<?php echo $actualizar_Ruta->lugar_destino_ruta; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Tiempo aproximado de la ruta:</label>
                  <input type="number" class="form-control" name="tiempo_aprox_ruta" id="tiempo_aprox_ruta" value="<?php echo $actualizar_Ruta->tiempo_aprox_ruta; ?>" required >
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-10">
                  <button type="submint" name="button" class="btn btn-success btn-lg">
                    Continuar
                  </button>
            </div>
              </form>
        </div>
</div>
<div class="col-md-3">
</div>
</div>
