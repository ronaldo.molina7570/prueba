<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Rutas</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->
    <div class="container">
      <br>
      <br>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar Ruta</button>
            <br>
            <center>
                <?php echo $this->session->userdata("usuario_Conectado")['perfil']  ?>
                <h2 style="font-weight:bold;">RUTAS REGISTRADAS</h2>
            </center>
            <br>
            <table class="table table-bordered table-striped table-hover" id="myTable">
                <thead>
                    <tr style="color:white">
                        <th class="text-center">
                            ID
                        </th>
                        <th class="text-center">
                            Lugar Salida
                        </th>
                        <th class="text-center">
                            lugar Destino
                        </th>
                        <th class="text-center">
                            Tiempo aproximado de la ruta
                        </th>
                        <th class="text-center">
                            ACCIONES
                        </th>
                    </tr>
                </thead>
                <tbody>
                  <?php if ($listado_rutas): ?>
                    <?php foreach ($listado_rutas->result() as $ruta_temporal): ?>
                      <tr style="color: #fff">
                              <td class="text-center"><?php echo $ruta_temporal->id_ruta ; ?></td>
                              <td class="text-center"><?php echo $ruta_temporal->lugar_salida_ruta; ?></td>
                              <td class="text-center"><?php echo $ruta_temporal->lugar_destino_ruta; ?></td>
                              <td class="text-center"><?php echo $ruta_temporal->tiempo_aprox_ruta; ?> minutos</td>
                              <td align="center">
                              <a href="<?php echo site_url(); ?>/Rutas/actualizarRutas/<?php echo $ruta_temporal->id_ruta; ?>">
                                EDITAR
                              </a>
                              <br>
                               <a href="<?php echo site_url(); ?>/Rutas/eliminarRuta/<?php echo $ruta_temporal->id_ruta; ?>" onclick="confirmation(event)">
                              ELIMINAR
                            </a>
                              </td>
                      </tr>
                    <?php endforeach?>
                    <?php endif; ?>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
            <!-- Modal content-->
        <form class=""  action="<?php echo site_url(); ?>/rutas/guardarRuta" method="post" id="formulario_nuevo_usuario">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> <b>NUEVA RUTA</b></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="col-md-12">
                <label for="">Lugar de salida:</label>
                <input type="text" class="form-control" name="lugar_salida_ruta" id="lugar_salida_ruta" value="" placeholder="Ingrese el lugar de salida" required autocomplete="off">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="">Lugar de llegada</label>
                <input type="text" class="form-control" name="lugar_destino_ruta" id="lugar_destino_ruta" placeholder="Ingrese el lugar de llegada" required autocomplete="off">								</div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="">Tiempo aproximado de llegada:</label>
                <input type="number" class="form-control" name="tiempo_aprox_ruta" id="tiempo_aprox_ruta" placeholder="Ingrese el tiempo aproximado en minutos." required autocomplete="off">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="row" align="center">
              <div class="col-md-12">
                <button type="submit" name="button" class="genric-btn danger default" >Guardar Ruta</button>
                <button type="button" name="button" class="genric-btn danger default"  onclick="cerrarModal();">Cancelar Acción</button>
              </div>
            </div>
            </form>
          </div>
        </div>

      </div>
    </div>
</main>
<script type="text/javascript">
  function cerrarModal(){
    $("#myModal").modal("hide");
  }
</script>
<script type="text/javascript">
$('#myTable').DataTable( {
  "language": {
            "url": "//cdn.datatables.net/plug-ins/1.11.5/i18n/es-ES.json"
        },
        dom: "lfrtipB",
     buttons: [{
     extend: 'collection',
     className: "btn-primary",
     text: 'Exportar',
     color: 'white',
     buttons:
     [{
     extend: "pdf", className: "btn-primary"
     }],
   }]
} );
</script>

<style media="screen">
thead tr{
  background: black !important
}
  .odd{
    background: black !important
  }
  .even{
    background: black !important
  }
</style>
<style media="screen">
 .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
  color: white !important;
}
.nice-select{
  background-color: black;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
  color: white !important;
  background-color: black;
}
.dataTables_wrapper .dataTables_filter input {
  background-color: white;
}
</style>
<!-- mensaje confirmar gaurdado de la ruta -->
  <?php if ($this->session->flashdata('confirmacion2')): ?>
    		<script type="text/javascript">
    		Swal.fire({
    			position: 'mid',
    			icon: 'success',
    			title: 'Datos de la ruta guardados correctamente',
    			showConfirmButton: false,
    			timer: 1500
    	})
    		</script>

<?php endif; ?>
<!-- fin del mensaje de confirmacion -->
<!-- mensaje confirmar actualizacion de la ruta -->
  <?php if ($this->session->flashdata('actualizacion2')): ?>
    		<script type="text/javascript">
    		Swal.fire({
    			position: 'mid',
    			icon: 'success',
    			title: 'Datos de la ruta actualizados correctamente',
    			showConfirmButton: false,
    			timer: 1500
    	})
    		</script>

<?php endif; ?>
<!-- fin del mensaje de confirmacion -->
<!-- mensaje confirmar actualizacion de la ruta -->
<?php if ($this->session->flashdata('eliminacion')): ?>
	<script type="text/javascript">

	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'DATOS ELIMINADOS CORRECTAMENTE',
		showConfirmButton: false,
		timer: 1500
})

	</script>
<?php endif; ?>
<!-- fin del mensaje de confirmacion -->

<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar',
	background: '#0C062E',
	color:'#FFF'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>
