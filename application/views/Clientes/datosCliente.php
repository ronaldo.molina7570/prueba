<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Clientes</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="container">
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar Cliente</button>
        <br>
        <center>
            <h2 style="font-weight:bold;">CLIENTES REGISTRADOS</h2>
        </center>
        <?php echo $this->session->userdata("usuario_Conectado")['id']  ?>

        <br>
        <table class="table table-bordered table-striped table-hover" id="myTable">
            <thead>
                <tr style="color:white">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        CEDULA
                    </th>
                    <th class="text-center">
                        NOMBRES
                    </th>
                    <th class="text-center">
                        APELLIDOS
                    </th>
                    <th class="text-center">
                        CORREO
                    </th>
                    <th class="text-center">
                        FECHA DE NACIMIENTO
                    </th>
                    <th class="text-center">
                        DIRECCIÓN
                    </th>
                    <th class="text-center">
                        TELÉFONO CELULAR
                    </th>
                    <th class="text-center">
                        ROL
                    </th>
                    <th class="text-center">
                        ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody>
              <?php if ($listado_clientes): ?>
                <?php foreach ($listado_clientes->result() as $cliente_temporal): ?>
                  <tr style="color: #fff">
                          <td class="text-center"><?php echo $cliente_temporal->id_cliente ; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->cedula_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->nombres_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->apellidos_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->correo_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->fecha_nacimiento_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->direccion_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->celular_cliente; ?></td>
                          <td class="text-center"><?php echo $cliente_temporal->rol_cliente; ?></td>
                          <td align="center">
                          <a href="<?php echo site_url(); ?>/Clientes/actualizarCliente/<?php echo $cliente_temporal->id_cliente; ?>">
                            EDITAR
                          </a>
                          <a href="<?php echo site_url(); ?>/Clientes/eliminar_cliente/<?php echo $cliente_temporal->id_cliente; ?>"
                            onclick="return confirm('Seguro de desea eliminar?')">
                          ELIMINAR
                        </a>
                          </td>
                  </tr>
                <?php endforeach?>
                <?php endif; ?>
            </tbody>
        </table>
      </div>
    </div>
    <br>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="formulario_nuevo_cliente">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> <b>NUEVO CLIENTE</b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Formulario de Nuevo Cliente</p>
        <br>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Nombres:</label>
            <input type="text" class="form-control" name="nombres_cliente" id="nombres_cliente" value="" placeholder="Ingrese sus Nombres" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Apellidos:</label>
            <input type="text" class="form-control" name="apellidos_cliente" id="apellidos_cliente" placeholder="Ingrese sus Apellidos" type="text" required autocomplete="off">								</div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">N° de cédula:</label>
            <input type="number" class="form-control" name="cedula_cliente" id="cedula_cliente" placeholder="Ingrese su Número de Cedula" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Correo Electrónico:</label>
            <input type="email" class="form-control" name="correo_cliente" id="correo_cliente" placeholder="Ingrese su Correo Electrónico" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Fecha de Nacimiento:</label>
            <input type="date" class="form-control" name="fecha_nacimiento_cliente" id="fecha_nacimiento_cliente" placeholder="Ingrese su Fecha de Nacimiento" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Género:</label>
            <select class="form-control" name="genero_cliente" id="genero_cliente">
              <option value="Masculino">Masculino</option>
              <option value="Femenino">Femenino</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Direccion exacta:</label>
            <input type="text" class="form-control" name="direccion_cliente" id="direccion_cliente" placeholder="Ingrese su Direccion exacta" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Número de Celular:</label>
            <input type="number" class="form-control" name="celular_cliente" id="celular_cliente" placeholder="Ingrese su Número de Celular" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Contraseña:</label>
            <input type="password" class="form-control" name="pass_cliente" id="pass_cliente" placeholder="Ingrese su contraseña" type="text" required autocomplete="off">
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <div class="row" align="center">
          <div class="col-md-12">
            <button type="submit" name="button" class="genric-btn danger default" >Guardar Cliente</button>
            <button type="button" name="button" class="genric-btn danger default"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
        </form>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
function cargarClientes(){
  $("#contenedor_listado_clientes").load('<?php echo site_url("Clientes/tablaClientes"); ?>');
}
cargarClientes();
</script>
<script type="text/javascript">
  function cerrarModal(){
    $("#myModal").modal("hide");
  }
</script>
