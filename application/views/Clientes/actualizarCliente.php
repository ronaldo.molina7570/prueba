<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Clientes</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->
    <div class="container">
    <br>
    <div class="row">
      <div class="col-md-12">
        <br>
        <div id="contenedor_listado_clientes">

        </div>
      </div>
    </div>
    <br>
</div>
</main>
<div class="row">
    <div class="col-md-12 text-center" >
        <legend style="color: #fff">
            ACTUALIZAR DATOS DEL CLIENTE
        </legend>
    </div>
    <br>
    <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <form  class="" action="<?php echo site_url(); ?>/Clientes/editar_cliente" method="post" id="formulario_nuevo_cliente">
                <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $editar_cliente->id_cliente; ?>" required >
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Nombres:</label>
                  <input type="text" class="form-control" name="nombres_cliente" id="nombres_cliente" value="<?php echo $editar_cliente->nombres_cliente; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Apellidos:</label>
                  <input type="text" class="form-control" name="apellidos_cliente" id="apellidos_cliente" value="<?php echo $editar_cliente->apellidos_cliente; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">N° de cédula:</label>
                  <input type="number" class="form-control" name="cedula_cliente" id="cedula_cliente" value="<?php echo $editar_cliente->cedula_cliente; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Correo Electrónico:</label>
                  <input type="email" class="form-control" name="correo_cliente" id="correo_cliente" value="<?php echo $editar_cliente->correo_cliente; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Fecha de Nacimiento:</label>
                  <input type="date" class="form-control" name="fecha_nacimiento_cliente" id="fecha_nacimiento_cliente" value="<?php echo $editar_cliente->fecha_nacimiento_cliente; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Género:</label>
                  <select class="form-control" name="genero_cliente" id="genero_cliente">
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Direccion exacta:</label>
                  <input type="text" class="form-control" name="direccion_cliente" id="direccion_cliente" value="<?php echo $editar_cliente->direccion_cliente; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Número de Celular:</label>
                  <input type="number" class="form-control" name="celular_cliente" id="celular_cliente" value="<?php echo $editar_cliente->celular_cliente; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Contraseña:</label>
                  <input type="password" class="form-control" name="pass_cliente" id="pass_cliente" value="<?php echo $editar_cliente->rol_cliente; ?>" required >
                </div>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <button type="submit" name="button" class="btn btn-success btn-lg">
                    Continuar
                  </button>
            </div>
              </form>
            <div class="col-md-3">

            </div>
        </div>
</div>
