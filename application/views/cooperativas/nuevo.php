<br>
<br>
<br>
<br>
<br>
<br>

  <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-4 text-center">
  </div>
	<br>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/cooperativas/guardarCooperativa" method="post" id="frm_nuevo_cooperativa">
      <table class="">
        <tr>
					<h3> <font color="white">NUEVA COOPERATIVA</font></h3><br>

          <td><label for=""><h4><font color="white">Nombre Cooperativa:</font></h4></label></td>
          <td><input type="text" name="nombre_coop" id="nombre_coop" class="form-control"
          value="" placeholder="Ingrese su cooperativa" required autocomplete="off"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="white">Ej. Cotopaxi</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="white">Propietario Cooperativa:</font><h4></label></td>
          <td><input type="text" name="propietario_coop" id="propietario_coop" class="form-control"
          value="" placeholder="Ingrese el propietario" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="white">Ej. Juan Carlos</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

    <tr>
      <td>&nbsp;</td>
      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/cooperativas/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
</div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nuevo_cooperativa").validate({
    rules:{
      cedula_usu:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_usu:{
        required:true
      },
      nombres_usu:{
        required:true
      },
      direccion_usu:{
        required:true
      },
      telefono_celular_usu:{
        required:true
      }
    },
    messages:{
      cedula_usu:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_usu:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_usu:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_usu:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_usu:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
