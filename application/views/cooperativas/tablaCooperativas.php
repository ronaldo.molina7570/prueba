<center><h3><font color="black"> MEDICAMENTOS REGISTRADO</font></h3></center><br>

<?php if ($listadoCooperativas): ?>
    <table class="table table-bordered table-striped table-primary">
    <thead>
      <tr>
        <th class="text-center">Nombre</th>
        <th class="text-center">Propietario</th>
        <th class="text-center">ACCIONES</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoCooperativas->result() as $cooperativaTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $cooperativaTemporal->id_coop ?></td>
          <td class="text-center"><?php echo $cooperativaTemporal->nombre_coop ?></td>
          <td class="text-center"><?php echo $cooperativaTemporal->propietario_coop ?></td>

          <td class="text-center">
            <a href="<?php echo site_url(); ?>/cooperativas/editar/<?php echo $busTemporal->id_bus; ?>"class="fa fa-edit" title="Editar">
                EDITAR</a>
                <a href="<?php echo site_url(); ?>/cooperativas/eliminarCooperativa/<?php echo $busTemporal->id_bus; ?>"
                    onclick="confirmation(event)">
                    <i class="fa fa-trash" title="ELIMINAR"></i>
                  ELIMINAR</a>
          </a>  </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron medicamentos registrados
  </div>
<?php endif; ?>
