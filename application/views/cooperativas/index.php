<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Cooperativas</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="hero-cap hero-cap2 text-center pt-80">
									<section class="games-section">
									<div class="container">
										<div class="row">
											<div class="col-md-12 text-center">
												<legend><h3><FONT COLOR="blue ">LISTADO DE COOPERATIVAS</FONT></h3></legend>
											</div>
										</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-right">
											<a href="<?php echo site_url(); ?>/cooperativas/nuevo" type="button" class="site-btn">AGREGAR NUEVO <i class="glyphicon glyphicon-plus"></i></a><br><br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<?php if ($listadoCooperativas): ?>
													<table class="table table-bordered table-striped table-danger" id="tbl-cooperativas"
													>
													<thead>
														<tr>
															<th class="text-center">ID</th>
															<th class="text-center">NOMBRE</th>
															<th class="text-center">PROPIETARIO</th>

															<th class="text-center">ACCIONES</th>
														</tr>
												</thead>
													<tbody>
														<?php foreach ($listadoCooperativas->result() as $cooperativaTemporal): ?>
															<tr>
																<td class="text-center"><?php echo $cooperativaTemporal->id_coop ?></td>
																<td class="text-center"><?php echo $cooperativaTemporal->nombre_coop ?></td>
																<td class="text-center"><?php echo $cooperativaTemporal->propietario_coop?></td>

																<td class="text-center">
																	<a href="<?php echo site_url(); ?>/cooperativas/editar/<?php echo $cooperativaTemporal->id_coop; ?>"class="fa fa-edit">
																			</a>
																			<br>
																			<a href="<?php echo site_url(); ?>/cooperativas/eliminarCooperativa/<?php echo $cooperativaTemporal->id_coop; ?>"
																					onclick="confirmation(event)">
																					<i class="fa fa-trash" title="ELIMINAR"></i>
																					</a>
																 </td>
															</tr>
														<?php endforeach; ?>
													</tbody>
												</table>
											<?php else: ?>
												<div class="alert alert-danger">
													No se encontraron cooperativas registrados
												</div>
											<?php endif; ?>

										</div>

									</div>
									</div>
									</section>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar',
	background: '#0C062E',
	color:'#FFF'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>
<script type="text/javascript">
$(document).ready( function () {
	$('#tbl-cooperativas').DataTable();
} );
</script>{
