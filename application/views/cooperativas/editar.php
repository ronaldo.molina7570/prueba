<br><br><br> <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2>
    <font color="blue">
         FORMULARIO DE EDITAR COOPERATIVAS</font></h2>
    </legend>
  </div>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/cooperativas/actualizar_Cooperativa" method="post" id="frm_editar_coop">
      <input type="hidden" name="id_coop" id="id_coop" class="form-control"
      value="<?php echo $cooperativaEditar->id_coop; ?>" placeholder="Ingrese su id"><br>
      <table class="">
        <tr>
          <td><label for=""><h4><font color="white">Nombre del copp :</font></h4></label></td>
          <td><input type="text" name="nombre_coop" id="nombre_coop" class="form-control"
          value="<?php echo $cooperativaEditar->nombre_coop; ?>" placeholder="Ingrese su cooperativa" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej.Cotopaxi</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="white">Propietario cooperativa :</font><h4></label></td>
          <td><input type="text" name="propietario_coop" id="propietario_coop" class="form-control"
          value="<?php echo $cooperativaEditar->propietario_coop; ?>" placeholder="Ingrese la cooperativa" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Emilio Alvarez</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

    <br>
    <br>

      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/cooperativas/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_editar_coop").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
