<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Usuarios</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="container">
    <br>
    <br>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar Usuario</button>
        <br>
        <center>
            <?php echo $this->session->userdata("usuario_Conectado")['perfil']  ?>
            <h2 style="font-weight:bold;">USUARIOS REGISTRADOS</h2>
        </center>
        <br>
        <table class="table table-bordered table-striped table-hover" id="myTable">
            <thead>
                <tr style="color:white">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        CEDULA
                    </th>
                    <th class="text-center">
                        NOMBRES
                    </th>
                    <th class="text-center">
                        APELLIDOS
                    </th>
                    <th class="text-center">
                        CORREO
                    </th>
                    <th class="text-center">
                        FECHA DE NACIMIENTO
                    </th>
                    <th class="text-center">
                        DIRECCIÓN
                    </th>
                    <th class="text-center">
                        TELÉFONO CELULAR
                    </th>
                    <th class="text-center">
                        ROL
                    </th>
                    <th class="text-center">
                        ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody>
              <?php if ($listado_usuarios): ?>
                <?php foreach ($listado_usuarios->result() as $usuario_temporal): ?>
                  <tr style="color: #fff">
                          <td class="text-center"><?php echo $usuario_temporal->id_usuario ; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->cedula_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->nombres_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->apellidos_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->correo_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->fecha_nacimiento_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->direccion_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->celular_usuario; ?></td>
                          <td class="text-center"><?php echo $usuario_temporal->rol_usuario; ?></td>
                          <td align="center">
                          <a href="<?php echo site_url(); ?>/Usuarios/actualizarUsuarios/<?php echo $usuario_temporal->id_usuario; ?>">
                            EDITAR
                          </a>
                          <a href="<?php echo site_url(); ?>/Usuarios/eliminar_usuario/<?php echo $usuario_temporal->id_usuario; ?>"
                            onclick="confirmation(event)">
                          ELIMINAR
                        </a>
                          </td>
                  </tr>
                <?php endforeach?>
                <?php endif; ?>
            </tbody>
        </table>
      </div>
    </div>
    <br>
</div>
<!-- Modal -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/usuarios/guardarUsuario" method="post" id="formulario_nuevo_usuario">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> <b>NUEVO USUARIO</b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Nombres:</label>
            <input type="text" class="form-control" name="nombres_usuario" id="nombres_usuario" value="" placeholder="Ingrese sus Nombres" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Apellidos:</label>
            <input type="text" class="form-control" name="apellidos_usuario" id="apellidos_usuario" placeholder="Ingrese sus Apellidos" type="text" required autocomplete="off">								</div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">N° de cédula:</label>
            <input type="number" class="form-control" name="cedula_usuario" id="cedula_usuario" placeholder="Ingrese su Número de Cedula" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Correo Electrónico:</label>
            <input type="email" class="form-control" name="correo_usuario" id="correo_usuario" placeholder="Ingrese su Correo Electrónico" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Fecha de Nacimiento:</label>
            <input type="date" class="form-control" name="fecha_nacimiento_usuario" id="fecha_nacimiento_usuario" placeholder="Ingrese su Fecha de Nacimiento" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Género:</label>
            <br>
            <select class="form-control" name="genero_usuario" id="genero_usuario">
              <option value="Masculino">Masculino</option>
              <option value="Femenino">Femenino</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <br>
            <br>
            <label for="">Direccion exacta:</label>
            <input type="text" class="form-control" name="direccion_usuario" id="pass_usuario" placeholder="Ingrese su Direccion exacta" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Número de Celular:</label>
            <input type="number" class="form-control" name="celular_usuario" id="celular_usuario" placeholder="Ingrese su Número de Celular" type="text" required autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label for="">Cargo:</label>
            <br>
            <select class="form-control" name="rol_usuario" id="rol_usuario">
              <option value="Secretaria">SECRETARIA</option>
              <option value="Administrador">ADMINISTRADOR</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <br>
            <br>
            <label for="">Contraseña:</label>
            <input type="password" class="form-control" name="pass_usuario" id="pass_usuario" placeholder="Ingrese su contraseña" type="text" required autocomplete="off">
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <div class="row" align="center">
          <div class="col-md-12">
            <button type="submit" name="button" class="genric-btn danger default" >Guardar Usuario</button>
            <button type="button" name="button" class="genric-btn danger default"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
        </form>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  function cerrarModal(){
    $("#myModal").modal("hide");
  }
</script>
<script type="text/javascript">
    	$("#formulario_nuevo_usuario").validate({
    		rules:{
          nombres_usuario:{
    				required:true
    			},
          apellidos_usuario:{
            required:true
          },
    			cedula_usuario:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10,
            remote:{
                        url:"<?php echo site_url('Usuarios/validarCedulaExistente'); ?>",
                        data:{
                          "cedula_usuario":function(){
                            return $("#cedula_usuario").val();
                          }
                        },
                        type:"post"
                    }
    			},
          correo_usuario:{
            required:true
          },
          fecha_nacimiento_usuario:{
            required:true
          },
          genero_usuario:{
            required:true
          },
    			direccion_usuario:{
    				required:true
    			},
    			celular_usuario:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10
    			},
          rol_usuario:{
            required: true
          },
          pass_usuario:{
            required: true
          }
    		},
    		messages:{
          nombres_usuario:{
    				required:"Por favor ingrese sus nombres"
    			},
          apellidos_usuario:{
            required:"Por favor ingrese sus apellidos"
          },
    			cedula_usuario:{
    				required:"Por favor ingrese su numero de cédula",
    				minlength:"La cedula debe tener minimo 10 digitos",
    				maxlength: "La cedula solo debe tener 10 digitos",
            remote:"La cedula ya existe"
    			},
          correo_usuario:{
            required:"Por favor ingrese sus correo electrónico"
          },
          fecha_nacimiento_usuario:{
            required:"Por favor ingrese sus fecha de nacimiento"
          },
          genero_usuario:{
            required:"Por favor ingrese su genero"
          },
    			direccion_usuario:{
    				required:"Por favor ingrese su dirección"
    			},
    			celular_usuario:{
    				required:"Por favor ingrese su numero de celular",
    				minlength:"El numero de celular debe tener minimo 10 digitos",
    				maxlength: "El numero de celular solo debe tener 10 digitos"
    			},
          rol_usuario:{
            required: "Seleccione el rol"
          },
          pass_usuario:{
            required: "Ingrese una contraseña"
          }
    		},

    	});

</script>
<script type="text/javascript">
$('#myTable').DataTable( {
  "language": {
            "url": "//cdn.datatables.net/plug-ins/1.11.5/i18n/es-ES.json"
        },
        dom: "lfrtipB",
     buttons: [{
     extend: 'collection',
     className: "btn-primary",
     text: 'Exportar',
     color: 'white',
     buttons:
     [{
     extend: "pdf", className: "btn-primary"
     }],
   }]
} );
</script>

<style media="screen">
thead tr{
  background: black !important
}
  .odd{
    background: black !important
  }
  .even{
    background: black !important
  }
</style>
<style media="screen">
 .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
  color: white !important;
}
.nice-select{
  background-color: black;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
  color: white !important;
  background-color: black;
}
.dataTables_wrapper .dataTables_filter input {
  background-color: white;
}
</style>

<!-- mensaje confirmar gaurdado de usuario -->
  <?php if ($this->session->flashdata('confirmacion1')): ?>
    		<script type="text/javascript">
    		Swal.fire({
    			position: 'mid',
    			icon: 'success',
    			title: 'DATOS GUARDADOS CORRECTAMENTE',
    			showConfirmButton: false,
    			timer: 1500
    	})
    		</script>

<?php endif; ?>
<!-- fin del mensaje de confirmacion -->
<!-- mensaje confirmar actualizacion de usuario -->
  <?php if ($this->session->flashdata('actualizacion')): ?>
    		<script type="text/javascript">
    		Swal.fire({
    			position: 'mid',
    			icon: 'success',
    			title: 'Datos del usuario actualizados correctamente',
    			showConfirmButton: false,
    			timer: 1500
    	})
    		</script>

<?php endif; ?>
<!-- fin del mensaje de confirmacion -->
<!-- mensaje confirmar eliminacio del usuario -->
<?php if ($this->session->flashdata('eliminacion')): ?>
	<script type="text/javascript">

	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'DATOS ELIMINADOS CORRECTAMENTE',
		showConfirmButton: false,
		timer: 1500
})

	</script>
<?php endif; ?>
<!-- fin del mensaje de confirmacion -->
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar',
	background: '#0C062E',
	color:'#FFF'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>
