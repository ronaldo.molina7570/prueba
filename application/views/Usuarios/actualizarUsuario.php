<main>
    <!--? Hero Start -->
    <div class="slider-area2">
        <div class="slider-height2 hero-overly d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center pt-80">
                            <h2>Usuarios</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="row">
    <div class="col-md-12 text-center" >
        <legend style="color: #fff">
            ACTUALIZAR DATOS DEL USUARIO
        </legend>
    </div>
</div>
    <br>
    <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <form  class="" action="<?php echo site_url(); ?>/Usuarios/actualizar_Usuario" method="post" id="formulario_nuevo_usuario">
                <input type="hidden" name="id_usuario" id="id_usuario" class="form-control" value="<?php echo $actualizar_Usuario->id_usuario; ?>" required >
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Nombres:</label>
                  <input type="text" class="form-control" name="nombres_usuario" id="nombres_usuario" value="<?php echo $actualizar_Usuario->nombres_usuario; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Apellidos:</label>
                  <input type="text" class="form-control" name="apellidos_usuario" id="apellidos_usuario" value="<?php echo $actualizar_Usuario->apellidos_usuario; ?>"  required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">N° de cédula:</label>
                  <input type="number" class="form-control" name="cedula_usuario" id="cedula_usuario" value="<?php echo $actualizar_Usuario->cedula_usuario; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Correo Electrónico:</label>
                  <input type="email" class="form-control" name="correo_usuario" id="correo_usuario" value="<?php echo $actualizar_Usuario->correo_usuario; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Fecha de Nacimiento:</label>
                  <input type="date" class="form-control" name="fecha_nacimiento_usuario" id="fecha_nacimiento_usuario" value="<?php echo $actualizar_Usuario->fecha_nacimiento_usuario; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Género:</label>
                  <select class="form-control" name="genero_usuario" id="genero_usuario">
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Direccion exacta:</label>
                  <input type="text" class="form-control" name="direccion_usuario" id="direccion_usuario" value="<?php echo $actualizar_Usuario->direccion_usuario; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Número de Celular:</label>
                  <input type="number" class="form-control" name="celular_usuario" id="celular_usuario" value="<?php echo $actualizar_Usuario->celular_usuario; ?>" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color:#fff">Cargo:</label>
                  <select class="form-control" name="rol_usuario" id="rol_usuario">
                    <option value="Secretaria">Secretaria</option>
                    <option value="Administrador">Administrador</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label for="" style="color: #fff">Contraseña:</label>
                  <input type="password" class="form-control" name="pass_usuario" id="pass_usuario" value="<?php echo $actualizar_Usuario->pass_usuario; ?>" required >
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-10">
                  <button type="submint" name="button" class="btn btn-success btn-lg">
                    Continuar
                  </button>
            </div>
              </form>
        </div>
</div>
<div class="col-md-3">
</div>
</div>
