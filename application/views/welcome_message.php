<main>
    <!--? slider Area Start-->
    <div class="slider-area">
        <div class="slider-active dot-style">
            <div class="single-slider slider-height hero-overly d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6">
                            <div class="hero__caption">
                                <span data-animation="fadeInLeft" data-delay=".4s">Bienvenidos</span>
                                <h1 data-animation="fadeInLeft" data-delay=".6s">Un placer Atenderlos</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->
		<div class="row">
			<div class="col-md-12">
			</div>
		</div>
</main>
