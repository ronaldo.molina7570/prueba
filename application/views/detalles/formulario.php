
<div class="row">
  <div class="col-md-12">
  </div>


    <form class="newsletter-form" action="<?php echo site_url(); ?>/detalles/insertarDetalle" method="post" id="frm_nuevo_detalle">
      <table class="">
        <tr>
					<center>
					  <legend><h3><font color="blue"> FORMULARIO DETALLE</font></h3><br></legend>
					</center>
          <td><label for=""><h5><font color="white">BUS:</font></h5></label></td>
          <td><select class="form-control" name="fk_id_bus" id="fk_id_bus" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoBuses): ?>
                <?php foreach ($listadoBuses->result() as $busTemporal): ?>
                  <option value="<?php echo $busTemporal->id_bus; ?>">
                    <?php echo $busTemporal->nombre_bus; ?> - <?php echo $busTemporal->numero_bus; ?> <?php echo $busTemporal->nombre_chofer_bus; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="white">COOPERATIVA:</font></h5></td>
          <td><select class="form-control" name="fk_id_coop" id="fk_id_coop" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoCooperativas): ?>
                <?php foreach ($listadoCooperativas->result() as $coopTemporal): ?>
                  <option value="<?php echo $coopTemporal->id_coop; ?>">
                     <?php echo $coopTemporal->propietario_coop; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="white">FECHA INICIO:</font></h5></label></td>
          <td><input type="date" name="fecha_inicio_deta" id="fecha_inicio_deta" value="" required class="form-control" required></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="white">FECHA FIN:</font></h5></label></td>
          <td><input type="date" name="fecha_fin_deta" id="fecha_fin_deta" value="" required class="form-control"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table><center>
    <button type="submit" name="button" class="btn btn-danger">GUARDAR DETALLE</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>" class="btn btn-danger">CANCELAR</a>
    </center></div>
</div>
  </div>
</form>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nuevo_detalle").validate({
    rules:{
			fk_id_bus:{
        required:true,
      },
			fk_id_coop:{
        required:true,
      },
      fecha_inicio_deta:{
        required:true,
      },
			fecha_fin_deta:{
        required:true,
      }

    },
    messages:{
			fk_id_bus:{
				required:"<br>Por favor seleccione el el bus",
			},
			fk_id_coop:{
				required:"<br>Por favor seleccione la cooperativa",
			},
      fecha_inicio_deta:{
        required:"<br>Por favor ingrese la fecha de inicio  detalle",
      },
			fecha_fin_deta:{
        required:"<br>Por favor ingrese la fecha de fin detalle",
      }
    },
      errorElement : 'span'
  });
</script>
