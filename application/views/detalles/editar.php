
  <section class="blog-page">
    <div class="container">
  <div class="row">
    <div class="col-md-12">
  <form class="newsletter-form" action="<?php echo site_url(); ?>/detalles/actualizarDetalle" method="post" id="frm_editar_detalle">
    <input type="hidden" name="id_deta" id="id_deta" class="form-control"
    value="<?php echo $detaEditar->id_deta; ?>" placeholder="Ingrese su id">
    <table class="">
      <tr>

				<h3><font color="blue" >EDITAR DETALLE</font></h3><br>
				<br>
				<br>
        <td><label for=""><h5><font color="black">BUS:</font></h5></label></td>
        <td><select class="form-control" name="fk_id_bus" id="fk_id_bus" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoBuses): ?>
                <?php foreach ($listadoBuses->result() as $busTemporal): ?>
                  <option value="<?php echo $busTemporal->id_bus; ?>">
                    <?php echo $busTemporal->nombre_bus; ?> - <?php echo $busTemporal->numero_bus; ?> <?php echo $busTemporal->nombre_chofer_bus; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="black">COOPERATIVA:</font></h5></td>
            <td><select class="form-control" name="fk_id_coop" id="fk_id_coop" required>
                <option value="">--Seleccione--</option>
                <?php if ($listadoCooperativas): ?>
                  <?php foreach ($listadoCooperativas->result() as $coopTemporal): ?>
                    <option value="<?php echo $coopTemporal->id_coop; ?>">
                       <?php echo $coopTemporal->propietario_coop; ?>
                    </option>
                  <?php endforeach; ?>
                <?php endif; ?>
            </select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">FECHA INICIO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h5></label></td>
        <td><input type="date" name="fecha_inicio_deta" id="fecha_inicio_deta" value="<?php echo $detalleEditar->fecha_inicio_deta; ?>" required class="form-control"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">FECHA FIN:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h5></label></td>
        <td><input type="date" name="fecha_fin_deta" id="fecha_fin_deta" value="<?php echo $detalleEditar->fecha_fin_deta; ?>" required class="form-control"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">NOMBRE:</font></h5></label></td>
        <td><input type="number" placeholder="Ingrese el precio" name="nombre_deta" id="nombre_deta" placeholder="Ingrese el nombre detalle " value="<?php echo $detalleEditar->nombre_deta; ?>" class="form-control" required></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table><center>
      <button type="submit" name="button" class="site-btn">GUARDAR DETALLE</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>" class="site-btn">CANCELAR</a><center>
<div>
  </div>
  </form>
</div>
</section>
  <script type="text/javascript">
    $("#fk_id_bus").val('<?php echo $detalleEditar->fk_id_bus; ?>');
  </script>
  <script type="text/javascript">
    $("#fk_id_coop").val('<?php echo $detalleEditar->fk_id_coop; ?>');
  </script>
<script type="text/javascript">
  $("#frm_editar_detalle").validate({
    rules:{
      costo_alquiler_pel:{
        required:true,
        digits:true,
        maxlength:3,
        minlength:1
      },
      fk_id_pel:{
        required:true
      }
    },
    messages:{
      costo_alquiler_pel:{
        required:"Por favor ingrese el precio",
        digits:"Por favor ingrese solo numeros",
        maxlength:"Por favor ingrese 3 digitos",
        minlength:"Por favor ingrese 3 digitos"
      },
      fk_id_pel:{
        required:"Por favor ingrese el genero"
      }
    }
  });
</script>
