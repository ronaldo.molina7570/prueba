
  <section class="blog-page">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <legend><h3><FONT COLOR="blue">LISTADO DE DETALLE</FONT></h3></legend>

    </div>

  </div>
<br>
<div class="row">
  <div class="col-md-12 text-right">
    <a href="<?php echo site_url(); ?>/detalles/formulario" type="button" class="btn btn-danger">AGREGAR NUEVO <i class="glyphicon glyphicon-plus"></i></a><br><br>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($listadoDetalles): ?>
        <table class="table table-bordered table-striped table-danger">
        <thead>
          <tr>
            <th class="text-center">Fecha de inicio</th>
            <th class="text-center">Fecha de finalizacion</th>
            <th class="text-center">Nombre bus</th>
            <th class="text-center">Numero del bus</th>
            <th class="text-center">Nombre del propietario</th>
            <th class="text-center">Acción</th>
          </tr>
      </thead>
        <tbody>
          <?php foreach ($listadoDetalles->result() as $detaTemporal): ?>
            <tr>
              <td class="text-center"><?php echo $detaTemporal->fecha_inicio_deta ?></td>
              <td class="text-center"><?php echo $detaTemporal->fecha_fin_deta ?></td>
              <td class="text-center"><?php echo $detaTemporal->nombre_bus ?></td>
              <td class="text-center"><?php echo $detaTemporal->numero_bus ?></td>
              <td class="text-center"><?php echo $detaTemporal->nombre_coop ?></td>
							<td class="text-center">
                <a href="<?php echo site_url(); ?>/detalles/editar/<?php echo $detaTemporal->id_deta; ?>"class="fa fa-edit" title="Editar">
                    EDITAR</a>
                    <a href="<?php echo site_url(); ?>/detalles/eliminarDetalle/<?php echo $detaTemporal->id_deta; ?>"
                        onclick="confirmation(event)">
                        <i class="fa fa-trash" title="ELIMINAR"></i>
                      ELIMINAR</a>

	              </a>  </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <div class="alert alert-danger">
        No se encontraron detalle registrados
      </div>
    <?php endif; ?>

  </div>

</div>
</div>
</section>
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar',
	background: '#0C062E',
	color:'#FFF'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>
