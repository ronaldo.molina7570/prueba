
  <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-4 text-center">
  </div>
	<br>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/buses/guardarBus" method="post" id="frm_nuevo_bus">
      <table class="">
        <tr>
					<h3> <font color="blue">NUEVO BUS</font></h3><br>

          <td><label for=""><h4><font color="white">Nombre del bus :</font></h4></label></td>
          <td><input type="text" name="nombre_bus" id="nombre_bus" class="form-control"
          value="" placeholder="Ingrese el nombre del bus " required autocomplete="off"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Cotopaxi</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="white">Número del bus :</font><h4></label></td>
          <td><input type="text" name="numero_bus" id="numero_bus" class="form-control"
          value="" placeholder="Ingrese el numero del bus " required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 78</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="white">Número de placa :</font></h4></label></td>
          <td><input type="text" name="numero_placa_bus" id="numero_placa_bus" class="form-control"
          value="" placeholder="Ingrese el numero de placa del bus " required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. xB 567</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="white">Nombre del chofer :</font></h4></label></td>
          <td><input type="text" name="nombre_chofer_bus" id="nombre_chofer_bus" class="form-control"
          value="" placeholder="Inngrese del nombre del chofer del bus" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej.Luis. </font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/buses/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
</div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nuevo_bus").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
