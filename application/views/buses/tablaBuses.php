<center><h3><FONT COLOR="blue">REGISTRO BUSES</FONT></h3><br></center>
<?php if ($listadoBuses): ?>
    <table class="table table-bordered table-striped table-danger">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE BUS</th>
        <th class="text-center">NÚMERO BUS</th>
        <th class="text-center">NUMERO PLACA</th>
        <th class="text-center">NOMBRE DEL CHOFER</th>
        <th class="text-center">ACCIONES</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoBuses->result() as $busTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $busTemporal->id_bus ?></td>
          <td class="text-center"><?php echo $busTemporal->nombre_bus ?></td>
          <td class="text-center"><?php echo $busTemporal->numero_bus ?></td>
          <td class="text-center"><?php echo $busTemporal->numero_placa_bus ?></td>
          <td class="text-center"><?php echo $busTemporal->nombre_chofer_bus ?></td>

          <td class="text-center">

              <a href="<?php echo site_url(); ?>/buses/editar/<?php echo $busTemporal->id_bus; ?>"class="fa fa-edit" title="Editar">
                  EDITAR</a>
                  <a href="<?php echo site_url(); ?>/buses/eliminarBus/<?php echo $busTemporal->id_bus; ?>"
                      onclick="confirmation(event)">
                      <i class="fa fa-trash" title="ELIMINAR"></i>
                    ELIMINAR</a>
            </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron usuarios registrados
  </div>
<?php endif; ?>
