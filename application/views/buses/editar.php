
  <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2>
    <font color="blue">
         FORMULARIO DE EDITAR BUS</font></h2>
    </legend>
  </div>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/buses/actualizarBus" method="post" id="frm_editar_bus">
      <input type="hidden" name="id_bus" id="id_bus" class="form-control"
      value="<?php echo $busEditar->id_bus; ?>" placeholder="Ingrese su id"><br>
      <table class="">
        <tr>
          <td><label for=""><h4><font color="white">Nombre del bus :</font></h4></label></td>
          <td><input type="text" name="nombre_bus" id="nombre_bus" class="form-control"
          value="<?php echo $busEditar->nombre_bus; ?>" placeholder="Ingrese el nombre del bus" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej.Cotopaxi</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="white">Numero del bus :</font><h4></label></td>
          <td><input type="number" name="numero_bus" id="numero_bus" class="form-control"
          value="<?php echo $busEditar->numero_bus; ?>" placeholder="Ingrese el numero del bus" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 34</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="white">Número de placa:</font></h4></label></td>
          <td><input type="number" name="numero_placa_bus" id="numero_placa_bus" class="form-control"
          value="<?php echo $busEditar->numero_placa_bus; ?>" placeholder="Ingrese el numero de la placa" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. xvt564</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
        <td><label for=""><h4><font color="white">Nombre del chofer:</font></h4></label></td>
          <td><input type="text" name="nombre_chofer_bus" id="nombre_chofer_bus" class="form-control"
          value="<?php echo $busEditar->nombre_chofer_bus; ?>" placeholder="Ingrese el nombre del chofer" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Pedro</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

    <br>
    <br>

      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/buses/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_editar_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
