
	<section class="page-top-section set-bg" data-setbg="<?php echo base_url(); ?>">
	</section>
	<!-- Page top end-->
  <section class="blog-page">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <legend><h3><FONT COLOR="blue">LISTADO DE BUSES</FONT></h3></legend>
    </div>
  </div>

<div class="row">
  <div class="col-md-12 text-right">
    <a href="<?php echo site_url(); ?>/buses/nuevo" type="button"
			class="site-btn">AGREGAR NUEVO <i class="glyphicon glyphicon-plus"></i></a>
		<button type="button" name="button" class="site-btn" onclick="cargarBuses();">
			actualizar datos
		</button>
	</div>
</div>
<br>
<div class="row">
  <div class="col-md-5 text-right">
    <center><h3><FONT COLOR="blue">NUEVO BUS</FONT></h3><br></center>
		<form class="" action="<?php echo site_url(); ?>/buses/guardarBus" method="post" id="frm_nuevo_bus">
      <table class="">
        <tr>
          <td><label for=""><h5><font color="white">Nombre del bus:</font></h5></label></td>
          <td><input type="text" name="nombre_bus" id="nombre_bus" class="form-control"
          value="" placeholder="Ingrese el nombre del bus" required autocomplete="off"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Cotopaxi</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="white">Número del bus:</font><h5></label></td>
          <td><input type="number" name="numero_bus" id="numero_bus" class="form-control"
          value="" placeholder="Ingrese el número del bus" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej.67</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h5><font color="white">Número de la placa:</font></h5></label></td>
          <td><input type="number" name="numero_placa_bus" id="numero_placa_bus" class="form-control"
          value="" placeholder="Ingrese el  número de la placa del bus " required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. XB456</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h5><font color="white">Nombre dek chofer:</font></h5></label></td>
          <td><input type="text" name="nombre_chofer_bus" id="nombre_chofer_bus" class="form-control"
          value="" placeholder="Ingrese el nombre del chofer del bus" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Luis </font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/buses/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center><br>
    </form>
  </div>

	<div class="col-md-7 text-right">

		<br>
		<div class="" id="contenedor_listado_buses">
		</div>
	</div>
</div>

<div class="row">
  <div class="col-md-12">


  </div>

</div>
</div>
</section>
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar',
	background: '#0C062E',
	color:'#FFF'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>

<style media="screen">
  span.error{ color: red; }
</style>

<script type="text/javascript">
  $("#frm_nuevo_bus").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		submitHandler:function(form){
          var url=$(form).prop("action");//capturando url (controlador/funcion)
          //generando peticion asincrona
          $.ajax({
               url:url,//action del formulario
               type:'post',//definiendo el tipo de envio de datos post/get
               data:$(form).serialize(), //enviando los datos ingresados en el formulario
               success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                  alert('Bus guardado exitosamente');
									cargarBuses(); //llamado a la funcion para actualizar listado de clientes
									$(form)[0].reset();
               },
               error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                  alert('Error al insertar, intente nuevamente');
               }
          });
        },
		errorElement : 'span'
  });
</script>
<script type="text/javascript">
	function cargarBuses(){
		$("#contenedor_listado_buses").load('<?php echo site_url("buses/tablaBuses") ?>')
	}
	cargarBuses();
</script>
