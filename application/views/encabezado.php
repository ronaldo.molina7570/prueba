<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Terminal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/slicknav.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<<<<<<< HEAD
=======
    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script type="text/javascript" 	src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
  	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!--DataTable -->
  	<link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
  	<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  	<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <!---jQuery UI -->
    <script type="text/javascript" src="https://releases.jquery.com/git/ui/jquery-ui-git.js"></script>
    <link rel="stylesheet" href="https://releases.jquery.com/git/ui/jquery-ui-git.css">
    <!---end jQuery UI -->

>>>>>>> 855e59a161a685d742610f8df460f3faf36a1dfd
</head>

<body class="body-bg">
<!--? Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="<?php echo base_url(); ?>/assets/img/logo/loder.jpg" alt="">
            </div>
        </div>
    </div>
</div>
<header>
    <!-- Header Start -->
    <div class="header-area header-transparent">
        <div class="main-header header-sticky">
            <div class="container-fluid">
                <div class="menu-wrapper d-flex align-items-center justify-content-between">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index.html"><img src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt=""></a>
                    </div>
                    <!-- Main-menu -->
                    <div class="main-menu f-right d-none d-lg-block">
                        <nav>
                            <ul id="navigation">
                                <li><a href="<?php echo site_url();?>">Inicio</a></li>
                                <?php if ($this->session->userdata("usuario_Conectado")): ?>
                                  <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="ADMINISTRADOR"): ?>
                                      <li><a href="<?php echo site_url();?>/Usuarios/datosUsuarios">Gestión Usuarios</a></li>
                                  <?php endif; ?>
                                  <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="Secretaria"): ?>
                                      <li><a href="<?php echo site_url();?>/Boletos/plantilla">Gestión Boletos</a></li>
                                      <li><a href="<?php echo site_url();?>/buses/index">Gestión Buses</a></li>
                                      <li><a href="<?php echo site_url();?>/detalles/index">Gestión Detalles</a></li>
                                      <li><a href="blog.html">Boletos</a></li>
                                      <li><a href="<?php echo site_url();?>/Buses/index">Gestión Buses</a></li>
<<<<<<< HEAD
                                      <li><a href="<?php echo site_url();?>/Cooperativas/index">Cooperativas</a></li>
                                      <li><a href="blog.html">Boletos</a></li>
=======
>>>>>>> 855e59a161a685d742610f8df460f3faf36a1dfd
                                  <?php endif; ?>
                                <li><a href="<?php echo site_url(); ?>/Seguridades/cerrarSesion">
						                         <?php echo $this->session->userdata("usuario_Conectado")['email'] ?>
						                         Salir</a>
					                      </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- Header-btn -->
                    <div class="header-btns d-none d-lg-block f-right">
                      <?php else: ?>
                        <a class="btn header-btn" href="#" data-toggle="modal" data-target="#login"><i class="flaticon-padlock"></i> Iniciar Sesión</a>
                        <?php endif; ?>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>
<!-- Modal -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header tit-up">
        <h4 class="modal-title" style="color:#000000">Iniciar Sesión</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body customer-box row">
				<div class="col-md-12">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#Login" class="genric-btn danger default" data-toggle="tab">Usuario</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
            <!-- Login Usuarios -->
            <br>
						<div class="tab-pane active" id="Login">
							<form role="form" class="form-horizontal" action="<?php echo site_url(); ?>/Seguridades/autenticarUsuario"  method="post">
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Correo Electrónico:</label>
									<input class="form-control" type="email" name="correo_usuario" id="correo_usuario" placeholder="Ingrese su Correo Electrónico" type="text">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Contraseña:</label>
									<input class="form-control" type="password" name="pass_usuario" id="pass_usuario" placeholder="Ingrese su Contraseña">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-success btn-lg">
										Aceptar
									</button>
                  <button type="button" class="btn btn-danger btn-lg" onclick="cerrarModal();">
										Cancelar
                  </button>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
<<<<<<< HEAD
=======
  <style>
  .navbar-nav{
  	float: right;
  }
  .error{
  	color: red;
  	font-weight: normal;
  }
  input.error{
  	border: 1px solid red;
  }
  </style>
  <?php if ($this->session->flashdata('error')): ?>
		<script type="text/javascript">
		Swal.fire({
			position: 'mid',
			icon: 'error',
			title: 'Email o constraseña incorrecto',
			showConfirmButton: true,
			timer: 1500
	})
		</script>
	<?php endif; ?>

  <?php if ($this->session->flashdata('confirmacionUsuario')): ?>
		<script type="text/javascript">
		Swal.fire({
			position: 'mid',
			icon: 'success',
			title: 'Acceso exitoso, Bienvenido al sistema',
			showConfirmButton: false,
			timer: 1500
	})
		</script>
	<?php endif; ?>
>>>>>>> 855e59a161a685d742610f8df460f3faf36a1dfd
