<?php
class Usuarios extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('usuario');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuario_Conectado")) {
      $this->session->set_flashdata("error","Por favor Inicie Sesion");
      redirect("Seguridades/login");
    }else{
      if (!($this->session->userdata("usuario_Conectado")["perfil"]!="ADMINISTRADOR" || $this->session->userdata("usuario_Conectado")["perfil"]!="VENDEDOR")){
        redirect("Seguridades/cerrarSesion");
      }
    }
  }
  public function datosUsuarios(){
    $idUsuarioConectado=$this->session->userdata("usuario_Conectado")["id"];
    $data["listado_usuarios"]=$this->usuario->obtenerTodosPorIdUsuario($idUsuarioConectado);
    $this->load->view("encabezado");
    $this->load->view("Usuarios/datosUsuarios",$data);
    $this->load->view("pie");
  }
  public function actualizarUsuarios($id_usuario){
    $data["actualizar_Usuario"]=$this->usuario->actualizar_datos($id_usuario);
    $this->load->view("encabezado");
    $this->load->view("Usuarios/actualizarUsuario",$data);
    $this->load->view("pie");
  }
  //funcion para gauradar los datos del usuario
  public function guardarUsuario(){
    $nombre=$this->input->post('nombres_usuario');
    $apellido=$this->input->post('apellidos_usuario');
    $cedula=$this->input->post('cedula_usuario');
    $correo=$this->input->post('correo_usuario');
    $fecha_nacimiento=$this->input->post('fecha_nacimiento_usuario');
    $genero=$this->input->post('genero_usuario');
    $direccion=$this->input->post('direccion_usuario');
    $celular=$this->input->post('celular_usuario');
    $rol_usuario=$this->input->post('rol_usuario');
    $pass=$this->input->post('pass_usuario');
    //armado arreglo para ingreso de datos en la bd
    $datosNuevoUsuario=array
    (
      'nombres_usuario'=>$nombre,
      'apellidos_usuario'=>$apellido,
      'cedula_usuario'=>$cedula,
      'correo_usuario'=>$correo,
      'fecha_nacimiento_usuario'=>$fecha_nacimiento,
      'genero_usuario'=>$genero,
      'direccion_usuario'=>$direccion,
      'celular_usuario'=>$celular,
      'rol_usuario'=>$rol_usuario,
      'pass_usuario'=>$pass,
      'fk_id_usuario'=>$this->session->userdata("usuario_Conectado")["id"]

    );
    if ($this->usuario->insertar($datosNuevoUsuario)) {
        $this->session->set_flashdata("confirmacion1","Datos del usuario guardados correctamente");
        eviarEmail($correo, "Esta son sus datos para inicar sesion","Nombre de usuario: ".$correo."<br> Contraseña: ".$pass, "");
          redirect('Usuarios/datosUsuarios');
    }else{
      redirect('/');
    }
  }
  //eliminar usuario
  public function eliminar_usuario($id_usuario){
          if($this->usuario->eliminar_usuario($id_usuario)){
            $this->session->set_flashdata("eliminacion","Datos del usuario actualizados correctamente");
           redirect('Usuarios/datosUsuarios');
          }else{
              echo "error al eliminar";
          }
      }
      //funcion para editar datos del usuarios
          public function actualizar_Usuario(){
               $id_usuario=$this->input->post("id_usuario");//captura el id del usuario a editar
               $datosEditadosUsuario=array(
                     "nombres_usuario"=>$this->input->post('nombres_usuario'),
                     "apellidos_usuario"=>$this->input->post('apellidos_usuario'),
                     "cedula_usuario"=>$this->input->post('cedula_usuario'),
                     "correo_usuario"=>$this->input->post('correo_usuario'),
                     "fecha_nacimiento_usuario"=>$this->input->post('fecha_nacimiento_usuario'),
                     "genero_usuario"=>$this->input->post('genero_usuario'),
                     "direccion_usuario"=>$this->input->post('direccion_usuario'),
                     "celular_usuario"=>$this->input->post('celular_usuario'),
                     "rol_usuario"=>$this->input->post('rol_usuario'),
                     "pass_usuario"=>$this->input->post('pass_usuario')
               );
               if($this->usuario->actualizar($id_usuario,$datosEditadosUsuario)){
                 //mensaje flash para confirmar
                   $this->session->set_flashdata("actualizacion","Datos del usuario actualizados correctamente");
                   echo "";
                   redirect('Usuarios/datosUsuarios');
               }else{
                   echo "error al actualizar";
               }
           }


           public function actualizar_Cooperativa(){
                $id_usuario=$this->input->post("id_coop");//captura el id del usuario a editar
                $datosEditadosCooperativa=array(
                      "nombres_usuario"=>$this->input->post('nombre_coop'),
                      "apellidos_usuario"=>$this->input->post('propietario_coop'),
                );
                if($this->usuario->actualizar($id_coop,$datosEditadosCooperativa)){
                  //mensaje flash para confirmar
                    $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
                    echo "";
                    redirect('cooperativa/tablaCooperativas');
                }else{
                    echo "error al actualizar";
                }
                }
           public function validarCedulaExistente(){
            $cedula_usuario=$this->input->post('cedula_usuario');
            $usuarioExistente=$this->usuario->consultarUsuarioPorCedula($cedula_usuario);
            if($usuarioExistente){
              echo json_encode(FALSE);
            }else{
              echo json_encode(TRUE);
            }
          }
}
?>
