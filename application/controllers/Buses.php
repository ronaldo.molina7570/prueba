<?php
class Buses extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cliente
    $this->load->model('bus');
    

  }
/*Funcion que renderiza el listado
de cloientes*/
  public function index()
  {
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('buses/index');
    $this->load->view('pie');
  }
  public function tablaBuses()
  {
    $data["listadoBuses"]=$this->bus->obtenerTodos();
    $this->load->view('buses/tablaBuses',$data);//pasando parametros a la vista
  }
  public function nuevo(){
    //Cargando la vista nuevo
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('buses/nuevo');
    $this->load->view('pie');
  }
  public function guardarBus(){

    //capturar valores de la vista
      $nombre=$this->input->post('nombre_bus');
      $numero=$this->input->post('numero_bus');
      $placa=$this->input->post('numero_placa_bus');
      $chofer=$this->input->post('nombre_chofer_bus');

      /*echo $email;
      echo "<br>";
      echo $password;*/
      //armando arreglo para insertar datos en la BDD
      $datosNuevoBus=array(
        "nombre_bus"=>$nombre,
        "numero_bus"=>$numero,
        "numero_placa_bus"=>$placa,
        "nombre_chofer_bus"=>$chofer,
      );
      if ($this->bus->insertar($datosNuevoBus)) {
        //si es verdadero si se inserto
        //mensaje flash para confiramar inserccion de cliente
        $this->session->set_flashdata("confirmacion","Bus registrado exitosamente");
        redirect('buses/index');
      }else {
        //si es falso no se inserto
        echo "Bus no  guardado";
      }
  }

  function pruebaEmail(){
			echo "<h3>HOLA ENVIO DE EVIDENCIA</h3>";
			$url=base_url('uploads/peliculas/HY.png');//ojo poner el nombre de una img existente
			enviarEmail("jenny.tenelema5614@utc.edu.ec",
									"hola  ",
									"<h1 style='color:blue;'>BUENOS DIAS, </h1> <br> René - ".date('Y-m-d H:i:s'),
									$url);
		}
  //metodo para eliminar cliente recibiendo como parametro su id
  public function eliminarBus($id){
    //validando si la eliminacion se realiza o no
    if ($this->bus->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Bus eliminado exitosamente");
      redirect('buses/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  //renderizar el formulario de actualizacion
  public function editar($id){
    $data['busEditar']=$this->bus->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('buses/editar',$data);
    $this->load->view('pie');
  }
  //metodo para llamar a la actualizacion del modelo
  public function actualizarBus(){
    $id_bus=$this->input->post('id_bus');//captura el id del cliente a EDITAR
    $datosEditados=array(
      "nombre_bus"=>$this->input->post('nombre_bus'),
        "numero_bus"=>$this->input->post('numero_bus'),
        "numero_placa_bus"=>$this->input->post('numero_placa_bus'),
        "nombre_chofer_bus"=>$this->input->post('nombre_chofer_bus'),
    );
    if($this->bus->actualizar($id_bus,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Bus actualizado exitosamente");
      redirect('buses/index');
    }else{
      echo "Error al actualizar";
    }
  }
  }
?>
