<?php
class Cooperativas extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cooperativa
    $this->load->model('cooperativa');

  }
/*Funcion que renderiza el listado
de cloientes*/
  public function index()
  {
    $data["listadoCooperativas"]=$this->cooperativa->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('cooperativas/index',$data);
    $this->load->view('pie');
  }
  public function tablaCooperativas()
  {
    $data["listadoCooperativas"]=$this->cooperativa->obtenerTodos();
    $this->load->view('cooperativas/tablaCooperativas',$data);//pasando parametros a la vista
  }
  public function nuevo(){
    //Cargando la vista nuevo
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('cooperativas/nuevo');
    $this->load->view('pie');
  }

  public function guardarCooperativa(){

    //capturar valores de la vista
      $nombre=$this->input->post('nombre_coop');
      $propietario=$this->input->post('propietario_coop');

      /*echo $email;
      echo "<br>";
      echo $password;*/
      //armando arreglo para insertar datos en la BDD
      $datosNuevoCooperativa=array(
        "nombre_coop"=>$nombre,
        "propietario_coop"=>$propietario,

      );
      if ($this->cooperativa->insertar($datosNuevoCooperativa)) {
        //si es verdadero si se inserto
        //mensaje flash para confiramar inserccion de cliente
        $this->session->set_flashdata("confirmacion","Cooperativa registrado exitosamente");
        redirect('cooperativas/index');
      }else {
        //si es falso no se inserto
        echo "Cooperativa no guardado";
      }
  }
  //metodo para eliminar cooperativa recibiendo como parametro su id
  public function eliminarCooperativa($id){
    //validando si la eliminacion se realiza o no
    if ($this->cooperativa->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Cooperativa eliminado exitosamente");
      redirect('cooperativas/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  //renderizar el formulario de actualizacion
  public function editar($id){
    $data['cooperativaEditar']=$this->cooperativa->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('cooperativas/editar',$data);
    $this->load->view('pie');
  }
  //metodo para llamar a la actualizacion del modelo
  public function actualizar_Cooperativa(){
    $id_coop=$this->input->post('id_coop');//captura el id del cooperativa a EDITAR
    $datosEditados=array(
      "nombre_coop"=>$this->input->post('nombre_coop'),
      "propietario_coop"=>$this->input->post('propietario_coop')
    );
    if($this->cooperativa->actualizar($id_coop,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Cooperativa actualizado exitosamente");
      redirect('cooperativas/index');
    }else{
      echo "Error al actualizar";
    }
  }



  }
?>
