<?php
class Clientes extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('cliente');
  }
  public function plantilla(){
    $this->load->view("encabezado");
    $this->load->view("Clientes/plantilla");
    $this->load->view("pie");
  }
  public function datosCliente(){
    $data["listado_clientes"]=$this->cliente->obtener_datos();
    $this->load->view("encabezado");
    $this->load->view("Clientes/datosCliente",$data);
    $this->load->view("pie");
  }
  public function actualizarCliente($id_cliente){
    $data["editar_cliente"]=$this->cliente->actualizar_datos($id_cliente);
    $this->load->view("encabezado");
    $this->load->view("Clientes/actualizarCliente",$data);
    $this->load->view("pie");
  }
  //funcion para gauradar los datos del cliente
  public function guardarCliente(){
    $rol_cliente="CLIENTE";
    $nombre=$this->input->post('nombres_cliente');
    $apellido=$this->input->post('apellidos_cliente');
    $cedula=$this->input->post('cedula_cliente');
    $correo=$this->input->post('correo_cliente');
    $fecha_nacimiento=$this->input->post('fecha_nacimiento_cliente');
    $genero=$this->input->post('genero_cliente');
    $direccion=$this->input->post('direccion_cliente');
    $celular=$this->input->post('celular_cliente');
    $pass=$this->input->post('pass_cliente');
    //armado arreglo para ingreso de datos en la bd
    $datosNuevoCliente=array
    (
      'nombres_cliente'=>$nombre,
      'apellidos_cliente'=>$apellido,
      'cedula_cliente'=>$cedula,
      'correo_cliente'=>$correo,
      'fecha_nacimiento_cliente'=>$fecha_nacimiento,
      'genero_cliente'=>$genero,
      'direccion_cliente'=>$direccion,
      'celular_cliente'=>$celular,
      'pass_cliente'=>$pass,
      'rol_cliente'=>$rol_cliente

    );
    if ($this->cliente->insertar($datosNuevoCliente)) {
        $this->session->set_flashdata("confirmacion1","Datos del cliente guardados correctamente");
        eviarEmail($correo, "Esta son sus datos para inicar sesion","Nombre de usuario: ".$correo."<br> Contraseña: ".$pass, "");
          redirect('clientes/datosCliente');
    }else{
      redirect('/');
    }
  }
  //eliminar cliente
  public function eliminar_cliente($id_cliente){
          if($this->cliente->eliminar_cliente($id_cliente)){
            $this->session->set_flashdata("eliminacion","Datos del cliente actualizados correctamente");
           redirect('clientes/datosCliente');
          }else{
              echo "error al eliminar";
          }
      }
  //funcion para editar datos del cliente
      public function editar_cliente(){
           $id_cliente=$this->input->post("id_cliente");//captura el id del usuario a editar
           $datosEditados=array(
                 "nombres_cliente"=>$this->input->post('nombres_cliente'),
                 "apellidos_cliente"=>$this->input->post('apellidos_cliente'),
                 "cedula_cliente"=>$this->input->post('cedula_cliente'),
                 "correo_cliente"=>$this->input->post('correo_cliente'),
                 "fecha_nacimiento_cliente"=>$this->input->post('fecha_nacimiento_cliente'),
                 "genero_cliente"=>$this->input->post('genero_cliente'),
                 "direccion_cliente"=>$this->input->post('direccion_cliente'),
                 "celular_cliente"=>$this->input->post('celular_cliente'),
                 "pass_cliente"=>$this->input->post('pass_cliente')
           );
           if($this->cliente->actualizar($id_cliente,$datosEditados)){
             //mensaje flash para confirmar
               $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
               redirect('clientes/datosCliente');
           }else{
               echo "error al actualizar";
           }
       }
}

?>
