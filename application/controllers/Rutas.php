<?php
class Rutas extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('ruta');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuario_Conectado")) {
      $this->session->set_flashdata("error","Por favor Inicie Sesion");
      redirect("Seguridades/login");
    }else{
      if (!($this->session->userdata("usuario_Conectado")["perfil"]!="ADMINISTRADOR" || $this->session->userdata("usuario_Conectado")["perfil"]!="VENDEDOR")){
        redirect("Seguridades/cerrarSesion");
      }
    }
  }
  public function datosRutas(){
    $idUsuarioConectado=$this->session->userdata("usuario_Conectado")["id"];
    $data["listado_rutas"]=$this->ruta->obtenerTodosPorIdUsuario($idUsuarioConectado);
    $this->load->view("encabezado");
    $this->load->view("Rutas/datosRutas",$data);
    $this->load->view("pie");
  }
  public function actualizarRutas($id_ruta){
    $data["actualizar_Ruta"]=$this->ruta->actualizar_datos($id_ruta);
    $this->load->view("encabezado");
    $this->load->view("Rutas/actualizarRutas",$data);
    $this->load->view("pie");
  }
  //funcion para guardar datos de las rutas
  public function guardarRuta(){
    $lugar_salida_ruta=$this->input->post('lugar_salida_ruta');
    $lugar_destino_ruta=$this->input->post('lugar_destino_ruta');
    $tiempo_aprox_ruta=$this->input->post('tiempo_aprox_ruta');
    //armado arreglo para ingreso de datos en la bd
    $datosNuevaRuta=array
    (
      'lugar_salida_ruta'=>$lugar_salida_ruta,
      'lugar_destino_ruta'=>$lugar_destino_ruta,
      'tiempo_aprox_ruta'=>$tiempo_aprox_ruta,
      'fk_id_usuario'=>$this->session->userdata("usuario_Conectado")["id"]

    );
    if ($this->ruta->insertar($datosNuevaRuta)) {
        $this->session->set_flashdata("confirmacion2","Datos de la ruta guardados correctamente");
          redirect('Rutas/datosRutas');
    }else{
      redirect('/');
    }
  }
  //eliminar ruta
  public function eliminarRuta($id_ruta){
          if($this->ruta->eliminar_ruta($id_ruta)){
            $this->session->set_flashdata("eliminacion","Datos del usuario actualizados correctamente");
           redirect('Rutas/datosRutas');
          }else{
              echo "error al eliminar";
          }
      }
      //funcion para editar datos del usuarios
          public function actualizar_Ruta(){
               $id_ruta=$this->input->post("id_ruta");//captura el id del usuario a editar
               $datosEditadosRuta=array(
                     "lugar_salida_ruta"=>$this->input->post('lugar_salida_ruta'),
                     "lugar_destino_ruta"=>$this->input->post('lugar_destino_ruta'),
                     "tiempo_aprox_ruta"=>$this->input->post('tiempo_aprox_ruta')
               );
               if($this->ruta->actualizar($id_ruta,$datosEditadosRuta)){
                 //mensaje flash para confirmar
                   $this->session->set_flashdata("actualizacion2","Datos de la ruta actualizados correctamente");
                   echo "";
                   redirect('Rutas/datosRutas');
               }else{
                   echo "error al actualizar";
               }
           }
}
?>
