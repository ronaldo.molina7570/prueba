<?php
class Detalles extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cliente
    $this->load->model('bus');
    $this->load->model('cooperativa');
    $this->load->model('detalle');
    //verificar si existe o no alguien conectado

  }
  public function formulario(){
    $data['listadoBuses']=$this->bus->obtenerTodos();
    $data['listadoCooperativas']=$this->cooperativa->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('detalles/formulario',$data);
    $this->load->view('pie');
  }
  //Funcion para insertar nuevos ALQUILERES
  public function insertarDetalle(){

    $query = $this->db->get_where('cooperativa', array('id_coop' => $this->input->post("fk_id_coop")));
    foreach ($query->result() as $row)
    {
      $val= $row->nombre_coop;
    }
    $dataDetalle=array(
      "fk_id_bus"=>$this->input->post("fk_id_bus"),
      "fk_id_coop"=>$this->input->post("fk_id_coop"),
      "nombre_deta"=>$val,
      "fecha_inicio_deta"=>$this->input->post("fecha_inicio_deta"),
      "fecha_fin_deta"=>$this->input->post("fecha_fin_deta")
    );
    if ($this->detalle->insertar($dataDetalle)) {
      $this->session->set_flashdata('confirmacion','Detalle registradoexitosamente');
      redirect('/detalles/index');

    } else {
      // code...
      $this->session->set_flashdata('confirmacion','Error al procesar, INTENTE NUEVAMENTE');
      redirect('/detalles/index');
    }

  }
  public function index()
  {
    $data["listadoDetalles"]=$this->detalle->obtenerTodos();
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('detalles/index',$data);//pasando parametros a la vista
    $this->load->view('pie');
  }

  public function eliminarDetalle($id){
    //validando si la eliminacion se realiza o no
    if ($this->detalle->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Detalle eliminado exitosamente");
      redirect('detalles/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data['detalleEditar']=$this->detalle->obtenerPorId($id);
    $data['listadoBuses']=$this->bus->obtenerTodos();
    $data['listadoCooperativas']=$this->coop->obtenerTodos();
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('detalles/editar',$data);
    $this->load->view('pie');
  }
//metodo para llamar a la actualizacion del modelo
public function actualizarDetalle(){
  $id_deta=$this->input->post('id_deta');//captura el id del cliente a EDITAR
  $datosEditados=array(
    "fk_id_bus"=>$this->input->post("fk_id_bus"),
    "fk_id_coop"=>$this->input->post("fk_id_coop"),
    "nombre_coop"=>$this->input->post("nombre_coop"),
    "fecha_inicio_deta"=>$this->input->post("fecha_inicio_deta"),
    "fecha_fin_deta"=>$this->input->post("fecha_fin_deta"),
    "fecha_actualizacion_deta"=>date('Y-m-d H:i:s')



  );
  if($this->deta->actualizar($id_deta,$datosEditados)){
    $this->session->set_flashdata("confirmacion","Detalles actualizado exitosamente");
    redirect('detalles/index');
  }else{
    echo "Error al actualizar";
  }
}
}
