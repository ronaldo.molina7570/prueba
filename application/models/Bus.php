<?php
  class Bus extends CI_Model{
    //funcion para inserta nuevo clientes
    public function insertar($datosBus){
      return $this->db->insert('bus',$datosBus);
    }
    //funcion para consultar datos de BDD
    public function obtenerTodos(){
      $query=$this->db->get('bus');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //funcion para consultar datos de BDD por id
    public function obtenerPorId($id){
      $this->db->where('id_bus',$id);
      $query=$this->db->get('bus');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //metodo para eliminar cliente recibiendo
    //como parametro su idea
    //funcion para eliminar clientes
  public function eliminarPorId($id_bus)
  {
    $this->db->where('id_bus',$id_bus);
    return $this->db->delete('bus');
  }
  //funcion para procesar la actualizacion del clientes
  public function actualizar($id,$datosBus){
    $this->db->where('id_bus',$id);
    return $this->db->update('bus',$datosBus);
  }
  }
 ?>
