<?php
  class Detalle extends CI_Model{
    public function insertar($data){
      return $this->db->insert('detalle',$data);
    }
    public function obtenerTodos(){
      $this->db->join('bus','bus.id_bus=detalle.fk_id_bus');
      $this->db->join('cooperativa','cooperativa.id_coop=detalle.fk_id_coop');
      $query=$this->db->get('detalle');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function obtenerPorId($id){
      $this->db->where('id_deta',$id);
      $query=$this->db->get('detalle');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function eliminarPorId($id_deta)
    {
      $this->db->where('id_deta',$id_deta);
      return $this->db->delete('detalle');
    }
    public function actualizar($id,$datosDetalle){
      $this->db->where('id_deta',$id);
      return $this->db->update('detalle',$datosDetalle);
    }
  }
