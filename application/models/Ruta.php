<?php
class Ruta extends CI_Model{
  //funcion para insertar nueva ruta
          public function insertar($datosRutas){
              return $this->db->insert('ruta',$datosRutas);
          }
          public function obtenerTodosPorIdUsuario($id_usuario){
            $this->db->where("fk_id_usuario",$id_usuario);//filtrando de acuerdo al usuario conectado
            $query=$this->db->get('ruta');
            if ($query->num_rows()>0){
              return $query; //cuando SI hay registros en la bdd
            }else {
              return false; //cuando NO hay registros en la bdd
            }
          }
          //funcion para eliminar datos
          public function eliminar_ruta($id_ruta){
              $this->db->where("id_ruta ",$id_ruta );
              return $this->db->delete("ruta");
          }
          //funcion para obtener 1 solo dato
          public function actualizar_datos($id_ruta){
            $this->db->where("id_ruta",$id_ruta);
            $query = $this->db->get('ruta');
            if($query->num_rows()>0){
                //echo "resulado: ";
                //print_r($query->row());
                return $query->row(); //retorna solo 1 fila del registro
            }else{
                //echo "error";
                return false; //cuando no hay registro en la base de datos
            }
        }
        //actualizar
          public function actualizar($id_ruta,$datos_rutas){
              $this->db->where("id_ruta",$id_ruta);
              return $this->db->update('ruta',$datos_rutas);

          }
}
?>
