<?php
class Seguridad extends CI_Model{
  //funcion para autenticar usuarios ->email y contraseña
  public function consultarPorEmailPassword($email, $password){
    $this->db->where('correo_usuario',$email);
    $this->db->where('pass_usuario',$password);
    $query=$this->db->get('usuario');
    if ($query->num_rows()>0) {
      return $query->row();
    }else{
      return false;
    }
  }

  public function consultarPasswordCliente($email, $password){
    $this->db->where('correo_cliente',$email);
    $this->db->where('pass_cliente',$password);
    $query=$this->db->get('cliente');
    if ($query->num_rows()>0) {
      return $query->row();
    }else{
      return false;
    }
  }
  }
 ?>
