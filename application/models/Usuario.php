<?php
class Usuario extends CI_Model{
  //funcion para insertar nuevo cliente
          public function insertar($datosUsuario){
              return $this->db->insert('usuario',$datosUsuario);
          }
          //FUNCION PARA OBTENER LOS DATOS DE LA BD
        public function obtener_datos(){
            $query = $this->db->get('usuario');
            if($query->num_rows()>0){
                return $query; //cuadno la base de datos si tiene datos
            }else{
                return false; //cuando no hay registro en la base de datos
            }
        }
        public function obtenerTodosPorIdUsuario($id_usuario){
          $this->db->where("fk_id_usuario",$id_usuario);//filtrando de acuerdo al usuario conectado
          $query=$this->db->get('usuario');
          if ($query->num_rows()>0){
            return $query; //cuando SI hay registros en la bdd
          }else {
            return false; //cuando NO hay registros en la bdd
          }
        }
        //funcion para eliminar datos
        public function eliminar_usuario($id_usuario){
            $this->db->where("id_usuario ",$id_usuario );
            return $this->db->delete("usuario");
        }
        //funcion para obtener 1 solo dato
        public function actualizar_datos($id_usuario){
          $this->db->where("id_usuario",$id_usuario);
          $query = $this->db->get('usuario');
          if($query->num_rows()>0){
              //echo "resulado: ";
              //print_r($query->row());
              return $query->row(); //retorna solo 1 fila del registro
          }else{
              //echo "error";
              return false; //cuando no hay registro en la base de datos
          }
      }
      //actualizar
        public function actualizar($id_usuario,$datos_usuario){
            $this->db->where("id_usuario",$id_usuario);
            return $this->db->update('usuario',$datos_usuario);

        }
        public function consultarUsuarioPorCedula($cedula_usuario){
          $this->db->where('cedula_usuario',$cedula_usuario);
          $query=$this->db->get('usuario');
          if($query->num_rows()>0){
            return $query->row();
          }else{
            false;
          }
        }
}
?>
