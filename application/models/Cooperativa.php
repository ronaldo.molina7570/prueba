<?php
  class Cooperativa extends CI_Model{
    //funcion para inserta nuevo clientes
    public function insertar($datosCooperativa){
      return $this->db->insert('cooperativa',$datosCooperativa);
    }
    //funcion para consultar datos de BDD
    public function obtenerTodos(){
      $query=$this->db->get('cooperativa');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //funcion para consultar datos de BDD por id
    public function obtenerPorId($id){
      $this->db->where('id_coop',$id);
      $query=$this->db->get('cooperativa');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //metodo para eliminar cliente recibiendo
    //como parametro su idea
    //funcion para eliminar cooperativas
  public function eliminarPorId($id_coop)
  {
    $this->db->where('id_coop',$id_coop);
    return $this->db->delete('cooperativa');
  }
  //funcion para procesar la actualizacion del clientes
  public function actualizar($id,$datosCooperativa){
    $this->db->where('id_coop',$id);
    return $this->db->update('cooperativa',$datosCooperativa);
  }


  }
 ?>
